# spack_ska

This repository contains spack recipes for SKA (Square Kilometre Array) software.

### How to use

Download Spack and source it:
```
git clone https://github.com/spack/spack.git
source ./spack/share/spack/setup-env.sh
```

Clone this repo:
```
git clone https://gitlab.com/scitas/radio-astronomical-imaging/spack_ska.git
```

Create a spack environment, activate it. Then add the repository as a spack repo and add the packages you need.
```
spack env create ska_env
spack env activate ska_env -p
spack repo add ./spack-ska
spack add oskar@2.8.3+casacore+cuda+hdf5
```
Then install the environment:
```
spack install -j 16
```

### Note
For further details on how to use this repository (tcl modules etc.) check also: https://git.astron.nl/RD/schaap-spack
