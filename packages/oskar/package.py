# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *
import os

class Oskar(CMakePackage):
    """OSKAR: A GPU-accelerated simulator for the Square Kilometre Array"""

    homepage = "https://ska-telescope.gitlab.io/sim/oskar/index.html"
    git      = "https://github.com/OxfordSKA/OSKAR.git"

    version('2.8.3', commit='9396cc3a288ab69c51054139f70adbfe1afe8aad', submodules=True)
    version('latest', branch='master', submodules=True, preferred=True)
    
    variant('casacore', default=False, description='Enable CASACORE support')
    variant('cuda', default=False, description='Enable CUDA support')
    variant('hdf5', default=False, description='Enable HDF5 support')

    depends_on('casacore', when='+casacore')
    depends_on('cmake')
    depends_on('cuda', when='+cuda')
    depends_on('hdf5+cxx', when='+hdf5')

    def cmake_args(self):
        cuda_arch_var = ""
        try:
            cuda_arch_var = os.environ.get("CUDA_ARCH_OSKAR")
        except KeyError:
            print("Please set the environment variable CUDA_ARCH_OSKAR")
            cuda_arch_var = "ALL"

        spec = self.spec
        args = [
            self.define('CUDA_ARCH', "ALL"),
        ]
        return args
